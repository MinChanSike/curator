﻿using System;
using System.Collections;

namespace Curator.Api.Models
{
    public class ErrorModel
    {
        public string Name { get; set; }

        public int Code { get; set; }

        public string TraceIdentifier { get; set; }

        public string Path { get; set; }

        public string Source { get; set; }

        public string Message { get; set; }

        public IDictionary Data { get; set; }
    }
}
