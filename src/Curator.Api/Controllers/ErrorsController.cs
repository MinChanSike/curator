﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Curator.Api.Models;
using Microsoft.AspNetCore.Diagnostics;
using System.Collections.Generic;
using Curator.Core.Exceptions;

namespace Curator.Api.Controllers
{
    [ApiController]
    [Route("errors")]
    public class ErrorsController : ControllerBase
    {
        private static readonly Dictionary<string, int> ERROR_CODE_MAPPING = new Dictionary<string, int>
        { { nameof(ArgumentNullException), 400 },
            { nameof(NotFoundException), 400 },
        };

        private readonly ILogger<ErrorsController> m_logger;

        public ErrorsController(ILogger<ErrorsController> logger)
        {
            m_logger = logger;
        }

        [Route("{code}")]
        public IActionResult Error(int code)
        {
            var exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            var errorName = exceptionHandlerPathFeature?.Error?.GetType().Name;

            if (errorName != null && ERROR_CODE_MAPPING.ContainsKey(errorName))
                Response.StatusCode = ERROR_CODE_MAPPING[errorName];

            return new ObjectResult(new ErrorModel
            {
                Code = Response.StatusCode,
                Name = errorName,
                Path = exceptionHandlerPathFeature?.Path,
                TraceIdentifier = HttpContext.TraceIdentifier,
                Source = exceptionHandlerPathFeature?.Error.Source,
                Message = exceptionHandlerPathFeature?.Error.Message,
                Data = exceptionHandlerPathFeature?.Error.Data,
            });
        }
    }
}
