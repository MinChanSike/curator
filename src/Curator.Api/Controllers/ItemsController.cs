﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Curator.Core;
using Curator.Core.Exceptions;
using Curator.Data.Entities;
using Curator.Api.Models;

namespace Curator.Api.Controllers
{
    [ApiController]
    [Route("items")]
    public class ItemsController : ControllerBase
    {
        private readonly ItemsService m_itemsService;
        private readonly ILogger<ItemsController> m_logger;

        public ItemsController(ItemsService itemsService, ILogger<ItemsController> logger)
        {
            m_itemsService = itemsService;
            m_logger = logger;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult> Get(int id)
        {
            return Ok(await m_itemsService.FindAsync(id));
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await m_itemsService.ListAsync());
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ItemModel model)
        {
            var item = await m_itemsService.AddAsync(model.Name);
            return CreatedAtAction(nameof(Get), new { id = item.Id }, item);

        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            await m_itemsService.RemoveAsync(id);
            return NoContent();
        }
    }
}
