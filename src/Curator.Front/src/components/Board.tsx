import React, { useState, useEffect, FormEvent } from 'react';
import IItem from '../models/IItem';
import './Board.css';

const Board: React.FunctionComponent = () => {
  const [items, setItems] = useState<Array<IItem>>([]);
  const [newItem, setNewItem] = useState<IItem>({ Name: '' });

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/items`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify(newItem),
    }).then(response => response.json().then(data => data as IItem))
      .then(item => {
        setItems([...items, item]);
        setNewItem({ Name: '' });
      });
  };

  const onRemove = (item: IItem) => {
    fetch(`${process.env.REACT_APP_API_URL}/items/${item.Id}`, {
      headers: { 'Content-Type': 'application/json' },
      method: 'DELETE',
    }).then(_ => {
      setItems([...items.filter(i => i.Id !== item.Id)]);
    });
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/items`)
      .then(response => response.json().then(data => data as Array<IItem>))
      .then(items => setItems(items));
  }, []);

  return (
    <div className="container">
      <header className="head"><h1>My curation Board</h1></header>

      <section>
        <form onSubmit={onSubmit}>
          <div className="form-group row">
            <div className="col-sm-10">
              <input type="Name" className="form-control" id="Name" placeholder="New item name"
                onChange={e => setNewItem({ Name: e.target.value })}
                value={newItem.Name} />
            </div>
            <button type="submit" className="btn btn-primary col-sm-2">Add this item</button>
          </div>
        </form>
      </section>

      <hr />

      <section className="items">
        <div className="list-group">
          {items.map(item =>
            <div key={item.Id} className="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
              <span>{item.Id} - {item.Name}</span>
              <span>
                <span className="badge badge-primary badge-pill">{item.CreatedAt}</span>
                <span className="badge badge-danger badge-pill" onClick={onRemove.bind(null, item)}>Remove</span>
              </span>
            </div>
          )}
        </div>
      </section>
    </div>
  );
}

export default Board;
