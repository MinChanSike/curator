import IItem from '../models/IItem';

export class ItemsService {
  public static async List(): Promise<Array<IItem>> {
    const response = await fetch(`http://localhost:5000/items`);
    return response.json().then(data => data as Array<IItem>);
  }

  public static async Find(id: number): Promise<IItem> {
    const response = await fetch(`http://localhost:5000/items/${id}`);
    return response.json().then(data => data as IItem);
  }
}

export default ItemsService;