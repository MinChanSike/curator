﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Curator.Console.Commands;
using Curator.Core;
using Curator.Data.EntityFramework.Context;
using Curator.Data.EntityFramework.Sqlite;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;

namespace Curator.Console
{
    class Program
    {
        static int Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<CuratorContext>(services => new DesignTimeDbContextFactory().CreateDbContext(args))
                .AddSingleton<ItemsService>()
                .BuildServiceProvider();

            var app = new CommandLineApplication<CuratorCommand>();

            app.Conventions
                .UseDefaultConventions()
                .UseConstructorInjection(serviceProvider);

            return app.Execute(args);
        }
    }
}
