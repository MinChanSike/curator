using System;
using System.Threading.Tasks;
using Curator.Core;
using McMaster.Extensions.CommandLineUtils;

namespace Curator.Console.Commands
{
    [Subcommand(typeof(AddCommand), typeof(ListCommand), typeof(RemoveCommand))]
    public class CuratorCommand: IDisposable
    {
        public ItemsService ItemsService { get; }

        public CuratorCommand(ItemsService itemsService)
        {
            ItemsService = itemsService;
        }

        private Task<int> OnExecuteAsync(CommandLineApplication application)
        {
            application.ShowHelp();
            return Task.FromResult(0);
        }

        public void Dispose()
        {
            ItemsService.Dispose();
        }
    }
}
