using System;
using System.Threading.Tasks;
using Curator.Data.Entities;
using McMaster.Extensions.CommandLineUtils;

namespace Curator.Console.Commands
{
    [Command("remove")]
    public class RemoveCommand
    {
        public CuratorCommand Parent { get; set; }

        [Argument(0, name: "Item Id")]
        public int Id { get; set; }

        public async Task<int> OnExecuteAsync()
        {
            if (Parent == null) throw new ArgumentNullException(nameof(Parent));

            await Parent.ItemsService.RemoveAsync(Id);
            return 0;
        }
    }
}
