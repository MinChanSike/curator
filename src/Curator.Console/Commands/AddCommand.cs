using System;
using System.Threading.Tasks;
using Curator.Data.Entities;
using McMaster.Extensions.CommandLineUtils;

namespace Curator.Console.Commands
{
    [Command("add")]
    public class AddCommand
    {
        public CuratorCommand Parent { get; set; }

        [Argument(0, name: "Item name")]
        public string Name { get; set; }

        public async Task<int> OnExecuteAsync()
        {
            if (Parent == null) throw new ArgumentNullException(nameof(Parent));
            if (String.IsNullOrEmpty(Name)) throw new ArgumentNullException(nameof(Name));

            await Parent.ItemsService.AddAsync(Name);
            return 0;
        }
    }
}
