using System;
using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;
using System.Linq;

namespace Curator.Console.Commands
{
    [Command("list")]
    public class ListCommand
    {
        public CuratorCommand Parent { get; set; }

        public async Task<int> OnExecuteAsync(IConsole console)
        {
            if (Parent == null) throw new ArgumentNullException(nameof(Parent));
            if (console == null) throw new ArgumentNullException(nameof(console));

            var items = await Parent.ItemsService.ListAsync();
            items.ToList().ForEach(item => console.WriteLine(item));
            return 0;
        }
    }
}
