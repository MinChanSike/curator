﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Curator.Data.EntityFramework.Context;

namespace Curator.Data.EntityFramework.Mysql
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CuratorContext>
    {
        private readonly IConfiguration m_configuration;

        public DesignTimeDbContextFactory()
        {
            m_configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional : false)
                .Build();
        }

        public DesignTimeDbContextFactory(IConfiguration configuration)
        {
            m_configuration = configuration;
        }

        public CuratorContext CreateDbContext(string[] args)
        {
            var connectionString = m_configuration.GetConnectionString("Curator.Entities");

            var builder = new DbContextOptionsBuilder<CuratorContext>()
                .UseMySQL(connectionString, b => b.MigrationsAssembly(GetType().Assembly.FullName));

            var context = new CuratorContext(builder.Options);
            context.Database.EnsureCreated();
            return context;
        }
    }
}
