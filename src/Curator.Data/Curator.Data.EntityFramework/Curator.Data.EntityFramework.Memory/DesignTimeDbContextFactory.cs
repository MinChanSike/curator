﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Curator.Data.EntityFramework.Context;

namespace Curator.Data.EntityFramework.Memory
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<CuratorContext>
    {
        public DesignTimeDbContextFactory()
        { }

        public CuratorContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<CuratorContext>()
                .UseInMemoryDatabase(databaseName: $"Curator.Entities.{DateTime.Now.Ticks}");

            return new CuratorContext(builder.Options);
        }
    }
}
