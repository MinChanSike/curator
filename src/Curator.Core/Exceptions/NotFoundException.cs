using System;
using System.Runtime.Serialization;

namespace Curator.Core.Exceptions
{
    [System.Serializable]
    public class NotFoundException : Exception
    {
        public int Id_Item { get; }

        public NotFoundException(int idItem) : this(idItem, null) { }

        public NotFoundException(int idItem, Exception inner) : base($"Unknow Item with Id [{idItem}]", inner) { }

        protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
