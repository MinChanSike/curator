using System;
using Curator.Console.Commands;
using Curator.Core;
using Curator.Data.Entities;
using Curator.Data.EntityFramework.Memory;
using Moq;
using Xunit;

namespace Curator.Console.Tests
{
    public class ListCommandTests
    {
        public class OnExecuteMethod
        {
            [Fact]
            public async void ShouldThrowIfParentIsNull()
            {
                var subject = new ListCommand();
                await Assert.ThrowsAsync<ArgumentNullException>("Parent", () => subject.OnExecuteAsync(null));
            }

            [Fact]
            public async void ShouldThrowIfConsoleIsNull()
            {
                var subject = new ListCommand();
                var context = new DesignTimeDbContextFactory().CreateDbContext(null);
                var service = new ItemsService(context);

                subject.Parent = new CuratorCommand(service);

                await Assert.ThrowsAsync<ArgumentNullException>("console", () => subject.OnExecuteAsync(null));
            }

            [Fact]
            public async void ShouldRemoveItemById()
            {
                var console = new FakeConsole();
                console.OutMock
                    .Setup(writer => writer.WriteLine(It.IsAny<Item>()))
                    .Verifiable();

                var subject = new ListCommand();
                var context = new DesignTimeDbContextFactory().CreateDbContext(null);
                var service = new ItemsService(context);

                var item1 = context.Add(new Item { Name = "Item 1" });
                var item2 = context.Add(new Item { Name = "Item 2" });
                await context.SaveChangesAsync();

                subject.Parent = new CuratorCommand(service);

                var result = await subject.OnExecuteAsync(console);

                console.OutMock.Verify(writer => writer.WriteLine(item1.Entity));
                console.OutMock.Verify(writer => writer.WriteLine(item2.Entity));

                Assert.Equal(0, result);
            }
        }
    }
}
