using System;
using System.Linq;
using Curator.Console.Commands;
using Curator.Core;
using Curator.Core.Exceptions;
using Curator.Data.Entities;
using Curator.Data.EntityFramework.Memory;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.AssertExtensions;

namespace Curator.Console.Tests
{
    public class RemoveCommandTests
    {
        public class OnExecuteMethod
        {
            [Fact]
            public async void ShouldThrowIfParentIsNull()
            {
                var subject = new RemoveCommand();
                await Assert.ThrowsAsync<ArgumentNullException>("Parent", subject.OnExecuteAsync);
            }

            [Fact]
            public async void ShouldThrowIfTheItemIdIsUnknown()
            {
                var subject = new RemoveCommand();
                var context = new DesignTimeDbContextFactory().CreateDbContext(null);
                var service = new ItemsService(context);

                subject.Parent = new CuratorCommand(service);
                subject.Id = Int32.MaxValue;

                var exception = await Assert.ThrowsAsync<NotFoundException>(subject.OnExecuteAsync);
                Assert.Equal($"Unknow Item with Id [{Int32.MaxValue}]", exception.Message);
            }

            [Fact]
            public async void ShouldRemoveItemById()
            {
                var subject = new RemoveCommand();
                var context = new DesignTimeDbContextFactory().CreateDbContext(null);
                var service = new ItemsService(context);

                var item1 = context.Add(new Item { Name = "Item 1" });
                var item2 = context.Add(new Item { Name = "Item 2" });
                await context.SaveChangesAsync();

                subject.Parent = new CuratorCommand(service);
                subject.Id = item2.Entity.Id;

                var result = await AssertDifference.CountAsync(context.Items, -1, subject.OnExecuteAsync);

                var actual = await context.Items.LastAsync();
                Assert.Equal(item1.Entity, actual);

                Assert.Equal(0, result);
            }
        }
    }
}
