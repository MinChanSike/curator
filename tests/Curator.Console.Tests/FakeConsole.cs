using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using McMaster.Extensions.CommandLineUtils;
using Moq;

namespace Curator.Console.Tests
{
    public class FakeConsole : IConsole
    {
        public TextWriter Out => OutMock.Object;
        public Mock<TextWriter> OutMock { get; }

        public TextWriter Error => throw new NotImplementedException();

        public TextReader In => throw new NotImplementedException();

        public bool IsInputRedirected => throw new NotImplementedException();

        public bool IsOutputRedirected => throw new NotImplementedException();

        public bool IsErrorRedirected => throw new NotImplementedException();

        public ConsoleColor ForegroundColor
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }
        public ConsoleColor BackgroundColor
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public event ConsoleCancelEventHandler CancelKeyPress;

        public FakeConsole()
        {
            OutMock = new Mock<TextWriter>();
        }

        public void ResetColor() => throw new NotImplementedException();
    }
}
