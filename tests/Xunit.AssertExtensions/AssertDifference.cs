﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xunit.AssertExtensions
{
    public static class AssertDifference
    {
        public static TAction Count<T, TAction>(IEnumerable<T> enumerable, int count, Func<TAction> action)
        {
            var before = enumerable.Count();
            var actual = action();
            var after = enumerable.Count();
            
            Assert.Equal(count, after - before);

            return actual;
        }

        public async static Task<TAction> CountAsync<T, TAction>(IEnumerable<T> enumerable, int count, Func<Task<TAction>> action)
        {
            var before = enumerable.Count();
            var actual = await action();
            var after = enumerable.Count();
            
            Assert.Equal(count, after - before);

            return actual;
        }
    }
}
