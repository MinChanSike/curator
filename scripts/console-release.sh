#!/bin/bash

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --runtime-identifier|-r)
    RUNTIME_IDENTIFIER="$2"
    shift
    shift
    ;;
    --version|-v)
    VERSION="$2"
    shift
    shift
    ;;
    *)
    shift
    ;;
esac
done

dotnet publish src/Curator.Console -c Release --self-contained true -r $RUNTIME_IDENTIFIER -o ../../dist/$RUNTIME_IDENTIFIER
(cd dist/$RUNTIME_IDENTIFIER && zip $RUNTIME_IDENTIFIER.zip ./*)
aws s3 cp dist/$RUNTIME_IDENTIFIER/$RUNTIME_IDENTIFIER.zip s3://$AWS_S3_DEPLOYMENT_BUCKET/curator-console/$RUNTIME_IDENTIFIER/$VERSION.zip --acl public-read --region $AWS_REGION
aws s3 cp dist/$RUNTIME_IDENTIFIER/$RUNTIME_IDENTIFIER.zip s3://$AWS_S3_DEPLOYMENT_BUCKET/curator-console/$RUNTIME_IDENTIFIER/latest.zip --acl public-read --region $AWS_REGION
