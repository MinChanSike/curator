#!/bin/bash

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --version|-v)
    VERSION="$2"
    shift
    shift
    ;;
    *)
    shift
    ;;
esac
done

aws s3 cp s3://$AWS_S3_DEPLOYMENT_BUCKET/curator-console/index.html index.html --region $AWS_REGION
echo '<h2>'$VERSION'</h2><ul>' >> index.html
echo '<li><a href="linux-x64/'$VERSION'.zip">Linux x64</a></li>' >> index.html
echo '<li><a href="osx-x64/'$VERSION'.zip">MacOs x64</a></li>' >> index.html
echo '<li><a href="win-x64/'$VERSION'.zip">Windows x64</a></li>' >> index.html
echo '</ul>' >> index.html
aws s3 cp index.html s3://$AWS_S3_DEPLOYMENT_BUCKET/curator-console/index.html --acl public-read --region $AWS_REGION
