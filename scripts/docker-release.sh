#!/bin/bash

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --version|-v)
    VERSION="$2"
    shift
    shift
    ;;
    *)
    shift
    ;;
esac
done

dotnet publish src/Curator.Api -c Release -o ../../dist/api/

docker image build -t curator-api .
docker image tag curator-api jbuiss0n/curator-api:$VERSION
docker image tag curator-api jbuiss0n/curator-api:latest
docker image push jbuiss0n/curator-api:$VERSION
docker image push jbuiss0n/curator-api:latest
