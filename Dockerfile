FROM microsoft/dotnet

RUN apt-get update -y && \
    apt-get install

RUN mkdir /app
WORKDIR /app
COPY dist/api .

ENV PLATFORM=Docker
CMD dotnet Curator.Api.dll
